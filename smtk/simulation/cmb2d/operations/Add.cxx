#include "smtk/simulation/cmb2d/operations/Add.h"

#include "smtk/simulation/cmb2d/Metadata.h"

#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/mesh/core/Resource.h"
#include "smtk/model/Resource.h"
#include "smtk/project/Project.h"

#include <algorithm>
#include <cctype>

#define LogError(message)                                                                          \
  do                                                                                               \
  {                                                                                                \
    smtkErrorMacro(this->log(), "Error: " << message);                                             \
    return this->createResult(Add::Outcome::FAILED);                                               \
  } while (0)

#define LogWarning(message)                                                                        \
  do                                                                                               \
  {                                                                                                \
    smtkErrorMacro(this->log(), "Warning: " << message);                                           \
  } while (0)

namespace
{
const int OP_SUCCEEDED = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);
}

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

Add::Result Add::operateInternal()
{
  auto roleStrItem = this->parameters()->findString("role");

  std::string roleStr = roleStrItem->value();
  // Filter the string if it is provided
  if (!roleStr.empty())
  {
    Metadata::Role role = Metadata::strToRole(roleStr);
    if (role == Metadata::Role::NROLE)
    {
      LogWarning("Unrecognized role: " << roleStrItem->value());
    }
    else
    {
      roleStr = Metadata::roleToStr(role);
    }
  }
  // Try to deduce the role from the input resource
  else
  {
    auto resource = this->parameters()->findResource("resource")->value();
    if (std::dynamic_pointer_cast<smtk::attribute::Resource>(resource))
    {
      roleStr = Metadata::roleToStr(Metadata::ANALYSIS);
    }
    else if (std::dynamic_pointer_cast<smtk::model::Resource>(resource))
    {
      roleStr = Metadata::roleToStr(Metadata::MODEL);
    }
    else if (std::dynamic_pointer_cast<smtk::mesh::Resource>(resource))
    {
      roleStr = Metadata::roleToStr(Metadata::MESH);
    }
    else
    {
      LogError("Role could not be deduced");
    }
  }

  auto project = this->parameters()->associations()->valueAs<smtk::project::Project>();

  auto roleSet = project->resources().findByRole(roleStr);
  smtk::resource::ResourcePtr save;
  if (!roleSet.empty())
  {
    // If this is the mesh role and there is already a resource
    // allow overwriting resource in role
    if (Metadata::strToRole(roleStr) == Metadata::Role::MESH)
    {
      save = *(roleSet.begin());
      project->resources().remove(save);
    }
    else if (Metadata::isUniqueRole(roleStr))
    {
      LogError("Cannot add more than one resource with a role that is unique");
    }
  }

  // Set the role with the case fixed
  roleStrItem->setValue(roleStr);

  auto result = smtk::project::Add::operateInternal();

  if (save)
  {
    int outcome = result->findInt("outcome")->value();
    if (outcome != OP_SUCCEEDED)
    {
      project->resources().add(save, roleStr);
    }
  }

  return result;
}

} // namespace cmb2d
} // namespace simulation
} // namespace smtk
