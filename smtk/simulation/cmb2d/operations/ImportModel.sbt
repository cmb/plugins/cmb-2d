<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="4">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="cmb2d-import-model" Label="CMB 2D - Import Model" BaseType="operation">
      <BriefDescription>
        Imports a Model to be used in a CMB 2D Project for creating a mesh
      </BriefDescription>
      <AssociationsDef Name="project" NumberOfRequiredValues="1" Extensible="false" OnlyResources="true">
        <Accepts><Resource Name="smtk::project::Project"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>
        <File Name="filename"
              Label="2D Model File"
              ShouldExists="true"
              FileFilters="Planar Polygon Files (*.ppg)">
          <BriefDescription>Model file to import</BriefDescription>
        </File>
        <Void Name="overwrite" Label="Allow overwrite" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
          <BriefDescrition>Allow overwriting existing project assets</BriefDescrition>
        </Void>
      </ItemDefinitions>

    </AttDef>

    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(cmb2d-import-model)" BaseType="result">
      <ItemDefinitions>
        <Resource Name="resource" HoldReference="true">
          <Accepts>
            <Resource Name="smtk::session::polygon::Resource"/>
          </Accepts>
        </Resource>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
