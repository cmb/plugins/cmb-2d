#include "smtk/simulation/cmb2d/operations/Export.h"
#include "smtk/simulation/cmb2d/operations/Export_xml.h"

#include "smtk/simulation/cmb2d/Metadata.h"
#include "smtk/simulation/cmb2d/utility/AttributeUtils.h"

#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/mesh/core/Resource.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/Operation.h"
#include "smtk/project/Project.h"

#include <iostream>

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

namespace
{
const int OP_SUCCEEDED = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);
}

#define LogInternalError(message)                                                                  \
  do                                                                                               \
  {                                                                                                \
    smtkErrorMacro(this->log(), "Internal Error: " << message);                                    \
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);                        \
  } while (0)

#define LogError(message)                                                                          \
  do                                                                                               \
  {                                                                                                \
    smtkErrorMacro(this->log(), "Error: " << message);                                             \
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);                        \
  } while (0)

Export::Result Export::operateInternal()
{
  // Get the project
  auto refItem = this->parameters()->associations();
  auto project = refItem->valueAs<smtk::project::Project>();

  // Verify that all of the required elements to export the project are satisfied
  auto analysis_set =
    project->resources().findByRole(Metadata::roleToStr(Metadata::Role::ANALYSIS));
  if (analysis_set.empty())
  {
    LogError("Cannot export project without an analysis");
  }
  auto mesh_set = project->resources().findByRole(Metadata::roleToStr(Metadata::Role::MESH));
  if (mesh_set.empty())
  {
    LogError("Cannot export project without a mesh");
  }

  auto analysis = std::dynamic_pointer_cast<smtk::attribute::Resource>(*(analysis_set.begin()));
  if (!analysis)
  {
    LogError("Invalid analysis");
  }
  auto mesh = std::dynamic_pointer_cast<smtk::mesh::Resource>(*(mesh_set.begin()));
  if (!mesh)
  {
    LogError("Invalid mesh");
  }

  fs::path projectPath = Metadata::getProjectRoot(project);
  fs::path exportPath = projectPath / "export";
  if (!fs::exists(exportPath))
  {
    fs::create_directories(exportPath);
  }

  AttributeUtils attrUtils;
  auto analysisAtt = attrUtils.getAnalysisAtt(analysis);
  if (!analysisAtt)
  {
    LogError("Could not get Analysis Attribute");
  }
  smtk::attribute::ConstStringItemPtr simulationItem = analysisAtt->findString("Simulation");
  if (!simulationItem->isSet())
  {
    LogError("No analysis is set in the simulation attributes");
  }
  std::string analysisSimulation = simulationItem->value();

  // Call MinimalFEM Export
  if (analysisSimulation == Metadata::simulationToStr(Metadata::Simulation::MINIMALFEM))
  {
    std::string filename = "minfem.inp";
    fs::path exportFile = exportPath / filename;

    // TODO: Select Exporter from analysis
    auto exportOp = this->manager()->create("smtk::simulation::minimalfem::Export");
    if (!exportOp)
    {
      LogError("Could not export MinimalFEM simulation");
    }
    exportOp->parameters()->findResource("analysis")->setValue(analysis);
    exportOp->parameters()->findResource("mesh")->setValue(mesh);

    fs::path workflowRoot = Metadata::WORKFLOW_DIRECTORY;
    fs::path workflowDir = workflowRoot / analysisSimulation;
    exportOp->parameters()->findDirectory("workflow-directory")->setValue(workflowDir.string());
    exportOp->parameters()->findFile("filename")->setValue(exportFile.string());

    auto result = exportOp->operate();
    int exportOutcome = result->findInt("outcome")->value();

    return this->createResult(static_cast<Export::Outcome>(exportOutcome));
  }
  else
  {
    LogError("Unrecognized Simulation Analysis: " << analysisSimulation);
  }
}

const char* Export::xmlDescription() const
{
  return Export_xml;
}

} // namespace cmb2d
} // namespace simulation
} // namespace smtk
