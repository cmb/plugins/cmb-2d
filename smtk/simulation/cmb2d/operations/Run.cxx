#include "smtk/simulation/cmb2d/operations/Run.h"
#include "smtk/simulation/cmb2d/operations/Run_xml.h"

#include "smtk/simulation/cmb2d/Metadata.h"

#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/attribute/VoidItem.h"
#include "smtk/mesh/core/Resource.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/Operation.h"
#include "smtk/project/Project.h"

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

namespace
{
const int OP_SUCCEEDED = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);
}

#define LogInternalError(message)                                                                  \
  do                                                                                               \
  {                                                                                                \
    smtkErrorMacro(this->log(), "Internal Error: " << message);                                    \
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);                        \
  } while (0)

#define LogError(message)                                                                          \
  do                                                                                               \
  {                                                                                                \
    smtkErrorMacro(this->log(), "Error: " << message);                                             \
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);                        \
  } while (0)

Run::Result Run::operateInternal()
{
  // Get the project
  auto refItem = this->parameters()->associations();
  auto project = refItem->valueAs<smtk::project::Project>();

  // Verify that all of the required elements to export the project are satisfied
  auto analysis_set =
    project->resources().findByRole(Metadata::roleToStr(Metadata::Role::ANALYSIS));
  if (analysis_set.empty())
  {
    LogError("Cannot export project without an analysis");
  }
  auto mesh_set = project->resources().findByRole(Metadata::roleToStr(Metadata::Role::MESH));
  if (mesh_set.empty())
  {
    LogError("Cannot export project without a mesh");
  }

  auto analysis = std::dynamic_pointer_cast<smtk::attribute::Resource>(*(analysis_set.begin()));
  if (!analysis)
  {
    LogError("Invalid analysis");
  }
  auto mesh = std::dynamic_pointer_cast<smtk::mesh::Resource>(*(mesh_set.begin()));
  if (!mesh)
  {
    LogError("Invalid mesh");
  }

  bool allowOverwrite = this->parameters()->findVoid("overwrite")->isEnabled();

  // TODO: Select Export/Run from simulation analysis type

  // Export the simulation
  fs::path exportPath = Metadata::getProjectRoot(project) / "export";
  if (!fs::exists(exportPath))
  {
    auto exportOp = this->manager()->create("smtk::simulation::minimalfem::Export");
    if (!exportOp)
    {
      LogInternalError("Could not create minimalfem::Run operation");
    }
    exportOp->parameters()->findResource("analysis")->setValue(analysis);
    exportOp->parameters()->findResource("mesh")->setValue(mesh);

    // Workflow
    fs::path workflowRoot = Metadata::WORKFLOW_DIRECTORY;
    fs::path workflowDir = workflowRoot / "MinimalFEM";
    exportOp->parameters()->findDirectory("workflow-directory")->setValue(workflowDir.string());

    // Input File
    std::string filename = "minfem.inp";
    exportOp->parameters()->findFile("filename")->setValue((exportPath / filename).string());
    exportOp->parameters()->findVoid("overwrite")->setIsEnabled(allowOverwrite);
    auto result = exportOp->operate();
    if (result->findInt("outcome")->value() != OP_SUCCEEDED)
    {
      LogError("Failed to export simulation");
    }
  }

  // Run Simulation
  {
    auto runOp = this->manager()->create("smtk::simulation::minimalfem::Run");
    if (!runOp)
    {
      LogInternalError("Could not create minimalfem::Run operation");
    }

    runOp->parameters()->findDirectory("run-directory")->setValue(exportPath.string());
    runOp->parameters()->findVoid("overwrite")->setIsEnabled(allowOverwrite);
    auto result = runOp->operate();
    if (result->findInt("outcome")->value() != OP_SUCCEEDED)
    {
      LogError("Failed to run simulation");
    }
  }

  return this->createResult(static_cast<Run::Outcome>(Run::Outcome::SUCCEEDED));
}

const char* Run::xmlDescription() const
{
  return Run_xml;
}

} // namespace cmb2d
} // namespace simulation
} // namespace smtk
