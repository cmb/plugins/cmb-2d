#ifndef smtk_simulation_cmb2d_operations_Add_h
#define smtk_simulation_cmb2d_operations_Add_h

#include "smtk/simulation/cmb2d/Exports.h"

#include "smtk/project/operators/Add.h"

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

/** Operation to handle assigning project roles when adding to the CMB-2D project
 * This is a wrapper around the smtk::project::Add
 */
class SMTKCMB2D_EXPORT Add : public smtk::project::Add
{
public:
  smtkTypeMacro(smtk::simulation::cmb2d::Add);
  smtkCreateMacro(Add);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(smtk::project::Operation);

protected:
  Result operateInternal() override;
};

} // namespace cmb2d
} // namespace simulation
} // namespace smtk

#endif
