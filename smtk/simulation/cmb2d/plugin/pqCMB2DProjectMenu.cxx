//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqCMB2DProjectMenu.h"

// CMB2D Extension includes
#include "behaviors/pqImportModelBehavior.h"
#include "behaviors/pqMesherBehavior.h"
#include "behaviors/pqProjectCloseBehavior.h"
#include "behaviors/pqProjectExportBehavior.h"
#include "behaviors/pqProjectLoader.h"
#include "behaviors/pqProjectNewBehavior.h"
#include "behaviors/pqProjectOpenBehavior.h"
#include "behaviors/pqProjectSaveBehavior.h"
#include "behaviors/pqRunSimulationBehavior.h"
#include "behaviors/pqWindowsReadMeshBehavior.h"
#include "behaviors/pqWindowsReadModelBehavior.h"
#include "pqCMB2DRecentProjectsMenu.h"
#include "smtk/simulation/cmb2d/qt/qtProjectRuntime.h"
// TODO
//#include "pqRecentProjectsMenu.h"
// Save as Behavior...

#include "smtk/simulation/cmb2d/Metadata.h"
using smtk::simulation::cmb2d::Metadata;

// SMTK
#include "smtk/mesh/core/Resource.h"
#include "smtk/model/Resource.h"
#include "smtk/project/Project.h"
#include "smtk/project/ResourceContainer.h"

// ParaView
#include "pqApplicationCore.h"

#include <QAction>
#include <QDebug>
#include <QMenu>
#include <QString>
#include <QtGlobal> // qWarning()

#include <boost/filesystem.hpp>

#define REGEN_MESH_LABEL "Re-Generate Mesh..."
#define GEN_MESH_LABEL "Generate Mesh..."

//-----------------------------------------------------------------------------
pqCMB2DProjectMenu::pqCMB2DProjectMenu(QObject* parent)
  : Superclass(parent)
{
  this->startup();
}

//-----------------------------------------------------------------------------
pqCMB2DProjectMenu::~pqCMB2DProjectMenu()
{
  this->shutdown();
}

//-----------------------------------------------------------------------------
bool pqCMB2DProjectMenu::startup()
{
  auto pqCore = pqApplicationCore::instance();
  if (!pqCore)
  {
    qWarning() << "cannot initialize Project menu because pqCore is not found";
    return false;
  }

  // Access/create the singleton instances of our behaviors.
  auto newProjectBehavior = smtk::simulation::cmb2d::pqProjectNewBehavior::instance(this);
  auto loadProjectBehavior = smtk::simulation::cmb2d::pqProjectLoader::instance(this);
  auto saveProjectBehavior = smtk::simulation::cmb2d::pqProjectSaveBehavior::instance(this);
  auto closeProjectBehavior = smtk::simulation::cmb2d::pqProjectCloseBehavior::instance(this);

  auto importModelBehavior = smtk::simulation::cmb2d::pqImportModelBehavior::instance(this);
  auto mesherBehavior = smtk::simulation::cmb2d::pqMesherBehavior::instance(this);
  auto exportProjectBehavior = smtk::simulation::cmb2d::pqProjectExportBehavior::instance(this);
  auto runSimulationBehavior = smtk::simulation::cmb2d::pqRunSimulationBehavior::instance(this);

#ifdef WIN32
  auto windowsReadMeshBehavior = smtk::simulation::cmb2d::pqWindowsReadMeshBehavior::instance(this);
  auto windowsReadModelBehavior =
    smtk::simulation::cmb2d::pqWindowsReadModelBehavior::instance(this);
#endif

  // Initialize "New Project" action
  m_newProjectAction = new QAction(tr("New Project..."), this);
  auto newProjectReaction = new smtk::simulation::cmb2d::pqProjectNewReaction(m_newProjectAction);
  QObject::connect(
    newProjectBehavior,
    &smtk::simulation::cmb2d::pqProjectNewBehavior::projectCreated,
    this,
    &pqCMB2DProjectMenu::onProjectOpened);

  // Insert separator
  {
    QAction* sep = new QAction("", this);
    sep->setSeparator(true);
  }

  // Initialize "Open Project" action
  m_openProjectAction = new QAction(tr("Open Project..."), this);
  auto openProjectReaction =
    new smtk::simulation::cmb2d::pqProjectOpenReaction(m_openProjectAction);

  // Initialize "Recent Projects" menu
  m_recentProjectsAction = new QAction(tr("Recent Projects"), this);
  QMenu* menu = new QMenu();
  m_recentProjectsAction->setMenu(menu);
  m_recentProjectsMenu = new pqCMB2DRecentProjectsMenu(menu, menu);

  // Insert separator
  {
    QAction* sep = new QAction("", this);
    sep->setSeparator(true);
  }

#ifndef WIN32
  // Initialize "Import Model" action
  m_importModelAction = new QAction(tr("Import Geometry..."), this);
  m_importModelAction->setEnabled(false);
  auto importModelReaction =
    new smtk::simulation::cmb2d::pqImportModelReaction(m_importModelAction);
  QObject::connect(
    importModelBehavior,
    &smtk::simulation::cmb2d::pqImportModelBehavior::modelImported,
    this,
    &pqCMB2DProjectMenu::onProjectOpened);

  m_generateMeshAction = new QAction(tr(GEN_MESH_LABEL), this);
  m_generateMeshAction->setEnabled(false);
  auto mesherReaction = new smtk::simulation::cmb2d::pqMesherReaction(m_generateMeshAction);
  QObject::connect(
    mesherBehavior,
    &smtk::simulation::cmb2d::pqMesherBehavior::meshGenerated,
    this,
    &pqCMB2DProjectMenu::onMeshGenerated);
#else
  m_generateMeshAction = nullptr;

  m_windowsReadModelAction = new QAction(tr("Read Model Resource"), this);
  m_windowsReadModelAction->setEnabled(false);
  auto windowsReadModelReaction =
    new smtk::simulation::cmb2d::pqWindowsReadModelReaction(m_windowsReadModelAction);
  QObject::connect(
    windowsReadModelBehavior,
    &smtk::simulation::cmb2d::pqWindowsReadModelBehavior::modelRead,
    [this]() { this->testWindowsReadiness(); });

  m_windowsReadMeshAction = new QAction(tr("Read Mesh Resource"), this);
  m_windowsReadMeshAction->setEnabled(false);
  auto windowsReadMeshReaction =
    new smtk::simulation::cmb2d::pqWindowsReadMeshReaction(m_windowsReadMeshAction);
  QObject::connect(
    windowsReadMeshBehavior,
    &smtk::simulation::cmb2d::pqWindowsReadMeshBehavior::meshRead,
    [this]() { this->testWindowsReadiness(); });
#endif

  // Insert separator
  {
    QAction* sep = new QAction("", this);
    sep->setSeparator(true);
  }

  // Initialize "Export Project" action
  m_exportProjectAction = new QAction(tr("Export Analysis"), this);
  auto exportProjectReaction =
    new smtk::simulation::cmb2d::pqProjectExportReaction(m_exportProjectAction);
  QObject::connect(
    exportProjectBehavior,
    &smtk::simulation::cmb2d::pqProjectExportBehavior::simulationExported,
    this,
    &pqCMB2DProjectMenu::onSimulationExported);

  m_runSimulationAction = new QAction(tr("Run Solver"), this);
  auto runSimulationReaction =
    new smtk::simulation::cmb2d::pqRunSimulationReaction(m_runSimulationAction);

  // Insert separator
  {
    QAction* sep = new QAction("", this);
    sep->setSeparator(true);
  }

  // Initialize "Save Project" action
  m_saveProjectAction = new QAction(tr("Save Project"), this);
  auto saveProjectReaction =
    new smtk::simulation::cmb2d::pqProjectSaveReaction(m_saveProjectAction);

  // Initialize "Save As Project" action
  // m_saveAsProjectAction = new QAction(tr("Save As Project..."), this);
  // auto saveAsProjectReaction = new pqCMB2DProjectSaveAsReaction(m_saveAsProjectAction);

  // Insert separator
  {
    QAction* sep = new QAction("", this);
    sep->setSeparator(true);
  }

  // Initialize "Close Project" action
  m_closeProjectAction = new QAction(tr("Close Project"), this);
  auto closeProjectReaction =
    new smtk::simulation::cmb2d::pqProjectCloseReaction(m_closeProjectAction);
  QObject::connect(
    closeProjectBehavior,
    &smtk::simulation::cmb2d::pqProjectCloseBehavior::projectClosed,
    this,
    &pqCMB2DProjectMenu::onProjectClosed);

  // For now, presume that there is no project loaded at startup
  this->onProjectClosed();

  auto projectLoader = smtk::simulation::cmb2d::pqProjectLoader::instance(this);
  // Connect to project loader
  QObject::connect(
    projectLoader,
    &smtk::simulation::cmb2d::pqProjectLoader::projectOpened,
    this,
    &pqCMB2DProjectMenu::onProjectOpened);

  return true;
} // startup()

//-----------------------------------------------------------------------------
void pqCMB2DProjectMenu::shutdown()
{
  // Reserved for future use.
}

//-----------------------------------------------------------------------------
void pqCMB2DProjectMenu::onProjectOpened(smtk::project::ProjectPtr project)
{
  m_newProjectAction->setEnabled(false);
  m_openProjectAction->setEnabled(false);
  m_recentProjectsAction->setEnabled(false);
  m_saveProjectAction->setEnabled(true);
  m_closeProjectAction->setEnabled(true);

  auto model = project->resources().findByRole<smtk::model::Resource>(
    Metadata::getRoleStr(Metadata::Role::MODEL));
  auto mesh = project->resources().findByRole<smtk::mesh::Resource>(
    Metadata::getRoleStr(Metadata::Role::MESH));

#ifndef WIN32
  m_importModelAction->setEnabled(model.empty());
  if (mesh.empty())
  {
    m_generateMeshAction->setText(GEN_MESH_LABEL);
  }
  else
  {
    if (model.empty())
    {
      smtkWarningMacro(
        smtk::io::Logger::instance(), "Error: Detected a mesh but no model in project");
    }
    m_generateMeshAction->setText(REGEN_MESH_LABEL);
  }
  m_generateMeshAction->setEnabled(!model.empty());
#else
  m_windowsReadModelAction->setEnabled(model.empty());
  m_windowsReadMeshAction->setEnabled(mesh.empty());
#endif

  m_exportProjectAction->setEnabled(!mesh.empty());
  fs::path projectRoot = Metadata::getProjectRoot(project);
  m_runSimulationAction->setEnabled(!mesh.empty() && fs::exists(projectRoot / "export"));
}

void pqCMB2DProjectMenu::onMeshGenerated()
{
  m_newProjectAction->setEnabled(false);
  m_openProjectAction->setEnabled(false);
  m_recentProjectsAction->setEnabled(false);
  m_saveProjectAction->setEnabled(true);
  m_closeProjectAction->setEnabled(true);
  m_exportProjectAction->setEnabled(true);
  m_runSimulationAction->setEnabled(false);

#ifndef WIN32
  m_importModelAction->setEnabled(false);
  m_generateMeshAction->setText(REGEN_MESH_LABEL);
  m_generateMeshAction->setEnabled(true);
#endif
}

void pqCMB2DProjectMenu::onSimulationExported()
{
  m_newProjectAction->setEnabled(false);
  m_openProjectAction->setEnabled(false);
  m_recentProjectsAction->setEnabled(false);
  m_saveProjectAction->setEnabled(true);
  m_closeProjectAction->setEnabled(true);
  m_exportProjectAction->setEnabled(true);
  m_runSimulationAction->setEnabled(true);

#ifndef WIN32
  m_importModelAction->setEnabled(false);
  m_generateMeshAction->setEnabled(false);
#else
  m_windowsReadMeshAction->setEnabled(false);
  m_windowsReadModelAction->setEnabled(false);
#endif
}

//-----------------------------------------------------------------------------
void pqCMB2DProjectMenu::onProjectClosed()
{
  m_newProjectAction->setEnabled(true);
  m_openProjectAction->setEnabled(true);
  m_recentProjectsAction->setEnabled(true);
  m_saveProjectAction->setEnabled(false);
  m_closeProjectAction->setEnabled(false);
  m_exportProjectAction->setEnabled(false);
  m_runSimulationAction->setEnabled(false);
#ifndef WIN32
  m_importModelAction->setEnabled(false);
  m_generateMeshAction->setText(GEN_MESH_LABEL);
  m_generateMeshAction->setEnabled(false);
#else
  m_windowsReadModelAction->setEnabled(false);
  m_windowsReadMeshAction->setEnabled(false);
#endif
}

#ifdef WIN32
void pqCMB2DProjectMenu::testWindowsReadiness()
{
  auto project = smtk::simulation::cmb2d::qtProjectRuntime::instance()->project();
  auto resourceContainer = project->resources();

  auto model = project->resources().findByRole<smtk::model::Resource>(
    Metadata::getRoleStr(Metadata::Role::MODEL));
  auto mesh = project->resources().findByRole<smtk::mesh::Resource>(
    Metadata::getRoleStr(Metadata::Role::MESH));

  if (!model.empty())
  {
    m_windowsReadModelAction->setEnabled(false);
  }
  if (!mesh.empty())
  {
    m_windowsReadMeshAction->setEnabled(false);
    m_exportProjectAction->setEnabled(true);
    fs::path projectRoot = Metadata::getProjectRoot(project);
    m_runSimulationAction->setEnabled(fs::exists(projectRoot / "export"));
  }
}
#endif
