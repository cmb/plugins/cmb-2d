//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqCMB2DRecentProjectsMenu.h"

#include "smtk/simulation/cmb2d/plugin/behaviors/pqProjectLoader.h"

#include "pqApplicationCore.h"
#include "pqRecentlyUsedResourcesList.h"
#include "pqServer.h"
#include "pqServerConfiguration.h"
#include "pqServerResource.h"
#include "pqWaitCursor.h"

#include <QAction>
#include <QDebug>
#include <QList>
#include <QMap>
#include <QMenu>
#include <QString>

//=============================================================================
pqCMB2DRecentProjectsMenu::pqCMB2DRecentProjectsMenu(QMenu* menu, QObject* p)
  : QObject(p)
  , m_menu(menu)
{
  QObject::connect(m_menu, &QMenu::aboutToShow, this, &pqCMB2DRecentProjectsMenu::buildMenu);
  QObject::connect(m_menu, &QMenu::triggered, this, &pqCMB2DRecentProjectsMenu::onOpenProject);
}

//-----------------------------------------------------------------------------
pqCMB2DRecentProjectsMenu::~pqCMB2DRecentProjectsMenu() {}

//-----------------------------------------------------------------------------
void pqCMB2DRecentProjectsMenu::buildMenu()
{
  if (!m_menu)
  {
    return;
  }

  m_menu->clear();
  auto loader = smtk::simulation::cmb2d::pqProjectLoader::instance();

  // Get the set of all resources in most-recently-used order ...
  const pqRecentlyUsedResourcesList::ListT& resources =
    pqApplicationCore::instance()->recentlyUsedResources().list();

  // Sort resources to cluster them by servers.
  typedef QMap<QString, QList<pqServerResource>> ClusteredResourcesType;
  ClusteredResourcesType clusteredResources;

  for (int cc = 0; cc < resources.size(); cc++)
  {
    // Filter out resources not marked by our extension
    const pqServerResource& resource = resources[cc];
    if (!loader->canLoad(resource))
    {
      continue;
    }

    // Filter out anything from a remote server
    pqServerConfiguration config = resource.configuration();
    if (!config.isNameDefault())
    {
      continue;
    }

    // Add submenu item
    QString label = resource.toURI();
    if (label.startsWith("builtin:"))
    {
      label = label.mid(8);
    }
    QAction* const act = new QAction(label, m_menu);
    act->setData(resource.serializeString());
    m_menu->addAction(act);
  } // for (cc)
}

//-----------------------------------------------------------------------------
void pqCMB2DRecentProjectsMenu::onOpenProject(QAction* action)
{
  QString data = action ? action->data().toString() : QString();
  if (!data.isEmpty())
  {
    pqServerResource resource(action->data().toString());
    pqWaitCursor cursor;
    smtk::simulation::cmb2d::pqProjectLoader::instance()->load(resource);
  }
}
