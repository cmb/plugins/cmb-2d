#include "smtk/simulation/cmb2d/plugin/pqSelectDirectoryDialog.h"

// ParaView
#include "pqActiveObjects.h"
#include "pqCoreUtilities.h"
#include "pqFileDialog.h"
#include "pqServer.h"

#include <QAction>
#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QMessageBox>
#include <QPushButton>
#include <QSharedPointer>
#include <QString>

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

struct pqSelectDirectoryDialog::Internals
{
  Internals(pqServer* server)
    : m_server(server)
  {
    if(!m_server)
    {
      m_server = pqActiveObjects::instance().activeServer();
    }
  }

  pqServer* server() { return m_server; }
  std::string directory() { return m_selected_directory; }

  std::string m_selected_directory;
  pqServer* m_server;
};

pqSelectDirectoryDialog::pqSelectDirectoryDialog(QObject* parent, pqServer* server)
  : Superclass(parent)
  , m_internals(new Internals(server))
{
}

bool pqSelectDirectoryDialog::exec()
{
  // Initialize confirmation pop-up dialog in case the selected directory is non-empty
  QSharedPointer<QMessageBox> confirmDialog =
    QSharedPointer<QMessageBox>(new QMessageBox(pqCoreUtilities::mainWidget()));
  confirmDialog->setWindowTitle("OK to Delete Directory Contents?");
  confirmDialog->setText("The contents of this directory will be deleted. Continue?");
  const char* info =
    "You have selected an existing directory that contains one or more files or subdirectories."
    " Before creating the new directory, the current contents of that directory will be deleted "
    "permanently."
    " Click \"Continue\" if you wish to delete the current contents and create a new directory.\n";
  confirmDialog->setInformativeText(info);
  auto yesButton = confirmDialog->addButton("Continue", QMessageBox::AcceptRole);
  auto retryButton = confirmDialog->addButton("Change Directory", QMessageBox::ResetRole);
  auto noButton = confirmDialog->addButton("Cancel", QMessageBox::RejectRole);
  confirmDialog->setDefaultButton(QMessageBox::No);

  // Todo get optional start directory from settings
  QString workspaceFolder = QDir::currentPath();

  std::unique_ptr<pqFileDialog> fileDialog;
  const QString fileDialogTitle("Select or Create a New Directory");
  while (true)
  {
    // Must reinitialize file dialog each iteration in order to clear selected files
    fileDialog.reset();
    fileDialog = std::unique_ptr<pqFileDialog>(
      new pqFileDialog(m_internals->server(), pqCoreUtilities::mainWidget(), fileDialogTitle, workspaceFolder));
    fileDialog->setFileMode(pqFileDialog::Directory);

    if (fileDialog->exec() != QDialog::Accepted)
    {
      return false;
    }
    auto fileName = fileDialog->getSelectedFiles()[0];

    // Check if selected directory exists (and is non-empty) or not
    QDir filePath(fileName);
    // Clear the directory if user confirms so
    if (filePath.exists() && !filePath.isEmpty())
    {
      QString text;
      QTextStream qs(&text);
      qs << "The contents of directory \"" << filePath.dirName() << "\" will be deleted. Continue?";
      confirmDialog->setText(text);

      confirmDialog->exec();
      if (confirmDialog->clickedButton() == yesButton)
      {
        qDebug() << "Deleting contents of" << filePath.dirName();
        if (!filePath.removeRecursively())
        {
          QMessageBox::warning(
            pqCoreUtilities::mainWidget(),
            "Failed to delete directory contents",
            "The application could not delete the directory contents.",
            QMessageBox::Close);
          return false;
        }
        if (!filePath.mkdir(filePath.absolutePath()))
        {
          QMessageBox::warning(
            pqCoreUtilities::mainWidget(),
            "Failed to create directory",
            "The application could not create the directory.",
            QMessageBox::Close);
          return false;
        }
      }
      else if (confirmDialog->clickedButton() == retryButton)
      {
        fileDialog->selectFile(workspaceFolder);
        continue;
      }
      else // (noButton)
      {
        return false;
      }
    } // if

    // Get the separator ('/' for Unix and '\' for Windows)
    const QChar separator = filePath.separator();
    // Assign the parent and subdirectories
    // QString projectFolder = fileName.section(separator, -1);
    workspaceFolder = fileName.section(separator, 0, -2); // for next loop

    m_internals->m_selected_directory = filePath.absolutePath().toStdString();
    break;
  } // while
  return true;
}

std::string pqSelectDirectoryDialog::directory()
{
  return m_internals->directory();
}

} // namespace cmb2d
} // namespace simulation
} // namespace smtk
