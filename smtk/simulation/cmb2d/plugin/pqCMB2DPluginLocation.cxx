//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/simulation/cmb2d/plugin/pqCMB2DPluginLocation.h"

#include "smtk/simulation/cmb2d/Metadata.h"

#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QtGlobal>

pqCMB2DPluginLocation::pqCMB2DPluginLocation(QObject* parent)
  : Superclass(parent)
{
}

pqCMB2DPluginLocation::~pqCMB2DPluginLocation() {}

void pqCMB2DPluginLocation::StoreLocation(const char* fileLocation)
{
  if (fileLocation == nullptr)
  {
    qDebug() << "Internal Error: pqCMB2DPluginLocation::StoreLocation() called with null pointer";
    return;
  }

  // Check whether or not this is an installed package
  QFileInfo fileInfo(fileLocation);
  QDir dirLocation(fileInfo.absoluteDir());

#ifndef NDEBUG
  qDebug() << "Plugin location: " << fileLocation;
  qDebug() << "Plugin directory: " << dirLocation.path();
#endif

  // Save starting directory for diagnostics
  QDir startingDir(dirLocation);

  // Look for path to simulation workflows
  // * Depends on superbuild-packaging organization
  // * Which is platform-dependent
  static const QStringList workflowRelativePaths = {
  // Apple installation paths
#if defined(__APPLE__)
    "../Resources/workflows/CMB2D",
#endif
    // Standard installation path, check these last
    "../../../share/cmb/workflows/CMB2D",
    "../../../share/workflows/CMB2D",
    // Development
    "../../../install/share/workflows/CMB2D"
  };

  // Check if directory exists
  bool exists;
  Q_FOREACH (const QString& relativePath, workflowRelativePaths)
  {
    if (!(exists = dirLocation.cd(relativePath)))
    {
#ifdef NDEBUG
      qDebug() << "No workflow directory found for starting directory" << startingDir.path()
               << "and relative path" << relativePath;
#endif
      dirLocation = startingDir;
    }
    else
    {
      break;
    }
  }
  if (exists)
  {
#ifndef NDEBUG
    qDebug() << "Setting workflows directory to" << dirLocation.path();
#endif
    smtk::simulation::cmb2d::Metadata::WORKFLOW_DIRECTORY = dirLocation.path().toStdString();
  }
  else
  {
    qCritical() << "Could not locate installed workflow directory...\n"
                << "Using the default path: "
                << smtk::simulation::cmb2d::Metadata::WORKFLOW_DIRECTORY.c_str();
  }
}
