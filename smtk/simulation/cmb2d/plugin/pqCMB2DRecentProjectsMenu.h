//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqCMB2DRecentProjectsMenu_h
#define pqCMB2DRecentProjectsMenu_h

#include <QObject>

#include <QPointer>

class QAction;
class QMenu;

/** \brief Adds a "Recently Used" submenu to the main Projects menu
  */
class pqCMB2DRecentProjectsMenu : public QObject
{
  Q_OBJECT

public:
  /**
  * Assigns the menu that will display the list of files
  */
  pqCMB2DRecentProjectsMenu(QMenu* menu, QObject* parent = nullptr);
  ~pqCMB2DRecentProjectsMenu() override;

private Q_SLOTS:
  void buildMenu();
  void onOpenProject(QAction*);

private:
  Q_DISABLE_COPY(pqCMB2DRecentProjectsMenu);

  QPointer<QMenu> m_menu;
};

#endif // pqCMB2DRecentProjectsMenu_h
