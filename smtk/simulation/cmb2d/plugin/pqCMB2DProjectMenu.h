//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_cmb2d_plugin_pqCMB2DProjectMenu_h
#define smtk_simulation_cmb2d_plugin_pqCMB2DProjectMenu_h

#include <QActionGroup>

#include "smtk/simulation/cmb2d/qt/Exports.h"

#include "smtk/PublicPointerDefs.h"

class QAction;

class pqCMB2DRecentProjectsMenu;

/** \brief Adds a "CMB2D" menu to the application main window
  */
class SMTKCMB2DQTEXT_NO_EXPORT pqCMB2DProjectMenu : public QActionGroup
{
  Q_OBJECT
  using Superclass = QActionGroup;

Q_SIGNALS:

public Q_SLOTS:
  void onProjectOpened(smtk::project::ProjectPtr project);
  void onProjectClosed();
  void onMeshGenerated();
  void onSimulationExported();
#ifdef WIN32
  void testWindowsReadiness();
#endif

public:
  pqCMB2DProjectMenu(QObject* parent = nullptr);
  ~pqCMB2DProjectMenu() override;

  bool startup();
  void shutdown();

private:
  QAction* m_newProjectAction = nullptr;
  QAction* m_openProjectAction = nullptr;
  QAction* m_recentProjectsAction = nullptr;
  QAction* m_saveProjectAction = nullptr;
  // QAction* m_saveAsProjectAction;  // not supported
  QAction* m_closeProjectAction = nullptr;
  QAction* m_exportProjectAction = nullptr;
  QAction* m_runSimulationAction = nullptr;
  QAction* m_importModelAction = nullptr;
  QAction* m_generateMeshAction = nullptr;
#ifdef WIN32
  QAction* m_windowsReadMeshAction = nullptr;
  QAction* m_windowsReadModelAction = nullptr;
#endif

  pqCMB2DRecentProjectsMenu* m_recentProjectsMenu;

  Q_DISABLE_COPY(pqCMB2DProjectMenu);
};

#endif // pqCMB2DProjectMenu_h
