//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_cmb2d_plugin_pqImportModelBehavior_h
#define smtk_simulation_cmb2d_plugin_pqImportModelBehavior_h

#include "smtk/PublicPointerDefs.h"

#include "pqReaction.h"

#include <QObject>

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

/// A reaction for importing a model into a project.
/// This class is HARD-CODED to only support importing a model for the
/// MODEL role.

class pqImportModelReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqImportModelReaction(QAction* parent);

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqImportModelReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqImportModelBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqImportModelBehavior* instance(QObject* parent = nullptr);
  ~pqImportModelBehavior() override;

  void importModel();

Q_SIGNALS:
  void modelImported(smtk::project::ProjectPtr);

protected:
  pqImportModelBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqImportModelBehavior);
};

} // namespace cmb2d
} // namespace simulation
} // namespace smtk

#endif
