//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqProjectCloseBehavior.h"

#include "smtk/simulation/cmb2d/plugin/behaviors/pqProjectSaveBehavior.h"
#include "smtk/simulation/cmb2d/qt/qtProjectRuntime.h"

// SMTK
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKResource.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/io/Logger.h"
#include "smtk/project/Manager.h"
#include "smtk/project/Project.h"

// // Client side
#include "pqActiveObjects.h"
#include "pqApplicationCore.h"
#include "pqCoreUtilities.h"
#include "pqObjectBuilder.h"
#include "pqServer.h"

#include <QAction>
#include <QDebug>
#include <QMessageBox>
#include <QtGlobal>

#include <vector>
namespace smtk
{
namespace simulation
{
namespace cmb2d
{

//-----------------------------------------------------------------------------
pqProjectCloseReaction::pqProjectCloseReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

void pqProjectCloseReaction::onTriggered()
{
  pqProjectCloseBehavior::instance()->closeProject();
}
//-----------------------------------------------------------------------------
static pqProjectCloseBehavior* g_instance = nullptr;

pqProjectCloseBehavior::pqProjectCloseBehavior(QObject* parent)
  : Superclass(parent)
{
}

pqProjectCloseBehavior* pqProjectCloseBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqProjectCloseBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

pqProjectCloseBehavior::~pqProjectCloseBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}

void pqProjectCloseBehavior::closeProject()
{
  // Get current project
  auto project = smtk::simulation::cmb2d::qtProjectRuntime::instance()->project();
  if (project == nullptr)
  {
    qWarning() << "Internal error - no active project.";
    return;
  }

  // Check if project is modified
  if (!project->clean())
  {
    QMessageBox msgBox;
    msgBox.setText("The project has been modified.");
    msgBox.setInformativeText("Do you want to save your changes?");
    auto buttons = QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel;
    msgBox.setStandardButtons(buttons);
    msgBox.setDefaultButton(QMessageBox::Save);

    int ret = msgBox.exec();
    if (ret == QMessageBox::Cancel)
    {
      return;
    }
    else if (ret == QMessageBox::Save)
    {
      if (!pqProjectSaveBehavior::instance()->saveProject())
      {
        return;
      }
    }
  } // if (project modified)

  // Access the active server and get the project manager
  pqServer* server = pqActiveObjects::instance().activeServer();
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
  auto projectManager = wrapper->smtkProjectManager();
  auto projectName = project->name();
  auto resManager = std::static_pointer_cast<smtk::resource::Resource>(project)->manager();

  // First mark all project resources for removal
  for (auto iter = project->resources().begin(); iter != project->resources().end(); ++iter)
  {
    auto resource = *iter;
    resource->setMarkedForRemoval(true);
  }

  // Then remove resources from resource manager
  for (auto iter = project->resources().begin(); iter != project->resources().end(); ++iter)
  {
    auto resource = *iter;
    resManager->remove(resource);
  }

  // Unset the active project
  smtk::simulation::cmb2d::qtProjectRuntime::instance()->unsetProject(project);

  // Must remove project from *both* resource manager & project manager
  resManager->remove(project);
  projectManager->remove(project);

  qInfo() << "Closed project" << projectName.c_str();
  Q_EMIT this->projectClosed();
} // closeProject()

} // namespace cmb2d
} // namespace simulation
} // namespace smtk
