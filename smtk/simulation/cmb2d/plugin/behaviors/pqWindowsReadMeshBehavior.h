//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_cmb2d_plugin_pqWindowsReadMeshBehavior_h
#define smtk_simulation_cmb2d_plugin_pqWindowsReadMeshBehavior_h

#include "smtk/PublicPointerDefs.h"

#include "pqReaction.h"

#include <QObject>

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

/// A reaction for reading a mesh into a project.
/// This class is HARD-CODED to only support reading a mesh for the
/// MESH role.

class pqWindowsReadMeshReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqWindowsReadMeshReaction(QAction* parent);

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqWindowsReadMeshReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqWindowsReadMeshBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqWindowsReadMeshBehavior* instance(QObject* parent = nullptr);
  ~pqWindowsReadMeshBehavior() override;

  void readResource();

Q_SIGNALS:
  void meshRead();

protected:
  pqWindowsReadMeshBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqWindowsReadMeshBehavior);
};

} // namespace cmb2d
} // namespace simulation
} // namespace smtk

#endif
