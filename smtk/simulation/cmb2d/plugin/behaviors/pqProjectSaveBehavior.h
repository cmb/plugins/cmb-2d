//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqProjectSaveBehavior_h
#define pqProjectSaveBehavior_h

#include "smtk/PublicPointerDefs.h"

#include "pqReaction.h"

#include <QObject>

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

/// A reaction for writing a resource manager's state to disk.
class pqProjectSaveReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqProjectSaveReaction(QAction* parent);

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqProjectSaveReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqProjectSaveBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqProjectSaveBehavior* instance(QObject* parent = nullptr);
  ~pqProjectSaveBehavior() override;

  bool saveProject();

protected:
  pqProjectSaveBehavior(QObject* parent = nullptr);

Q_SIGNALS:
  void projectSaved(smtk::project::ProjectPtr);

private:
  Q_DISABLE_COPY(pqProjectSaveBehavior);
};

} // namespace cmb2d
} // namespace simulation
} // namespace smtk

#endif
