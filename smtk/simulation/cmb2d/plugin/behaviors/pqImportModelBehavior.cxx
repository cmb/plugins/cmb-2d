//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqImportModelBehavior.h"

// CMB2D Extension includes
#include "smtk/simulation/cmb2d/Metadata.h"
#include "smtk/simulation/cmb2d/operations/ImportModel.h"
#include "smtk/simulation/cmb2d/qt/qtProjectRuntime.h"
using smtk::simulation::cmb2d::Metadata;

// SMTK includes
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/Definition.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/FileItemDefinition.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/io/Logger.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/Operation.h"
#include "smtk/operation/operators/ImportResource.h"
#include "smtk/project/Manager.h"
#include "smtk/project/Project.h"

// ParaView includes
#include "pqActiveObjects.h"
#include "pqCoreUtilities.h"
#include "pqFileDialog.h"
#include "pqServer.h"
#include "pqWaitCursor.h"

// Qt includes
#include <QAction>
#include <QDebug>
#include <QDialog>
#include <QMessageBox>
#include <QString>

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

namespace
{
const int OP_SUCCESS = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);
} // namespace

//-----------------------------------------------------------------------------
pqImportModelReaction::pqImportModelReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

void pqImportModelReaction::onTriggered()
{
  pqImportModelBehavior::instance()->importModel();
}

//-----------------------------------------------------------------------------
static pqImportModelBehavior* g_instance = nullptr;

pqImportModelBehavior::pqImportModelBehavior(QObject* parent)
  : Superclass(parent)
{
}

pqImportModelBehavior* pqImportModelBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqImportModelBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

pqImportModelBehavior::~pqImportModelBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}

void pqImportModelBehavior::importModel()
{
  auto project = smtk::simulation::cmb2d::qtProjectRuntime::instance()->project();
  if (project == nullptr)
  {
    qWarning() << "Cannot import model because no project currently loaded.";
    return;
  }

  pqServer* server = pqActiveObjects::instance().activeServer();
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
  auto opManager = wrapper->smtkOperationManager();

  // Import the model
  auto importOp = opManager->create<smtk::simulation::cmb2d::ImportModel>();

  // Open a file dialog
  QString location;
  {
    // Get the file filters from the operation
    smtk::attribute::DefinitionPtr def = importOp->parameters()->definition();
    int itemPosition = def->findItemPosition("filename");
    smtk::attribute::ItemDefinitionPtr itemDef = def->itemDefinition(itemPosition);
    auto fileItemDef = std::dynamic_pointer_cast<smtk::attribute::FileItemDefinition>(itemDef);
    std::string filters = fileItemDef->getFileFilters();

    // Construct a file dialog for the user to select the model file
    QString filter(filters.c_str());
    pqFileDialog fileDialog(
      server, pqCoreUtilities::mainWidget(), tr("Select Model File:"), QString(), filter);
    fileDialog.setObjectName("FileOpenDialog");
    fileDialog.setFileMode(pqFileDialog::ExistingFile);
    fileDialog.setShowHidden(true);
    if (fileDialog.exec() != QDialog::Accepted)
    {
      return;
    }
    location = fileDialog.getSelectedFiles()[0];
  }

  importOp->parameters()->associate(project);
  importOp->parameters()->findFile("filename")->setValue(location.toStdString());
  auto result = importOp->operate();
  int outcome = result->findInt("outcome")->value();
  if (outcome != OP_SUCCESS)
  {
    qWarning() << importOp->log().convertToString().c_str();
    QMessageBox::warning(
      pqCoreUtilities::mainWidget(),
      "Error",
      "Unable to import model file; see \"Output Messages\" for more info.");
    return;
  }

  Q_EMIT this->modelImported(project->shared_from_this());
} // importModel()

} // namespace cmb2d
} // namespace simulation
} // namespace smtk
