#ifndef smtk_simulation_cmb2d_plugin_behaviors_pqExportBehavior_h
#define smtk_simulation_cmb2d_plugin_behaviors_pqExportBehavior_h

#include "pqReaction.h"
#include "smtk/simulation/cmb2d/qt/qtInstanced.h"

#include <QObject>

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

class pqProjectExportReaction : public pqReaction
{
  Q_OBJECT
  using Superclass = pqReaction;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqProjectExportReaction(QAction* parent);

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqProjectExportReaction)
};

class pqProjectExportBehavior : public qtInstanced<pqProjectExportBehavior>
{
  Q_OBJECT
  using Superclass = qtInstanced<pqProjectExportBehavior>;
  friend Superclass;

public:
  ~pqProjectExportBehavior() override;

  void exportSimulation();

Q_SIGNALS:
  void simulationExported();

protected:
  pqProjectExportBehavior(QObject* parent = nullptr);
};

} // namespace cmb2d
} // namespace simulation
} // namespace smtk

#endif // smtk_simulation_cmb2d_plugin_behaviors_pqExportBehavior_h
