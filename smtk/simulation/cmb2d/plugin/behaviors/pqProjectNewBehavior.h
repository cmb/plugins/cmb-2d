//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_cmb2d_plugin_pqProjectNewBehavior_h
#define smtk_simulation_cmb2d_plugin_pqProjectNewBehavior_h

#include "smtk/PublicPointerDefs.h"

#include "pqReaction.h"

#include <QObject>

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

/// A reaction for writing a resource manager's state to disk.
class pqProjectNewReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /// Constructor. Parent cannot be NULL.
  pqProjectNewReaction(QAction* parent);
  ~pqProjectNewReaction() = default;

protected:
  /// Called when the action is triggered.
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqProjectNewReaction)
};

/// \brief Add a menu item for writing the state of the resource manager.
class pqProjectNewBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqProjectNewBehavior* instance(QObject* parent = nullptr);
  ~pqProjectNewBehavior() override;

  void newProject();

Q_SIGNALS:
  void projectCreated(smtk::project::ProjectPtr);

protected:
  pqProjectNewBehavior(QObject* parent = nullptr);

private:
  class Internal;
  Internal* m_internal;

  Q_DISABLE_COPY(pqProjectNewBehavior);
};

} // namespace cmb2d
} // namespace simulation
} // namespace smtk

#endif
