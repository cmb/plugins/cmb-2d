#include "pqMesherBehavior.h"

#include "smtk/simulation/cmb2d/Metadata.h"
#include "smtk/simulation/cmb2d/operations/Add.h"
#include "smtk/simulation/cmb2d/qt/qtProjectRuntime.h"

// SMTK
#include "smtk/PublicPointerDefs.h"
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKResource.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/io/Logger.h"
#include "smtk/model/Edge.h"
#include "smtk/model/Face.h"
#include "smtk/model/Model.h"
#include "smtk/model/Resource.h"
#include "smtk/model/Vertex.h"
#include "smtk/operation/Manager.h"
#include "smtk/project/Project.h"

// ParaView
#include "pqActiveObjects.h"
#include "pqApplicationCore.h"
#include "pqCoreUtilities.h"
#include "pqObjectBuilder.h"
#include "pqServer.h"

// Qt
#include <QAction>
#include <QDebug>
#include <QMessageBox>
#include <QtGlobal>

#include <set>

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

namespace
{
const int OP_SUCCESS = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);
} // namespace

pqMesherReaction::pqMesherReaction(QAction* parent)
  : Superclass(parent)
{
}

void pqMesherReaction::onTriggered()
{
  pqMesherBehavior::instance()->generateMesh();
}

//---------------------------------------

static pqMesherBehavior* g_instance = nullptr;

pqMesherBehavior::pqMesherBehavior(QObject* parent)
  : Superclass(parent)
{
}

pqMesherBehavior* pqMesherBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqMesherBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

pqMesherBehavior::~pqMesherBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}

void pqMesherBehavior::generateMesh()
{
  // Get current project
  auto project = smtk::simulation::cmb2d::qtProjectRuntime::instance()->project();
  if (project == nullptr)
  {
    qWarning() << "Internal error - no active project.";
    return;
  }

  // Check if project is modified
  if (!project->clean())
  {
    qInfo() << "TODO project is modified.";
  }

  // Access the active server and get the project manager
  pqServer* server = pqActiveObjects::instance().activeServer();
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
  auto opManager = wrapper->smtkOperationManager();

  // TODO: Select the mesher from the project analsys
  auto xmsGenMeshOp = opManager->create("smtk::mesh::xms::GenerateMesh");
  if (!xmsGenMeshOp)
  {
    QString msg;
    QTextStream qs(&msg);
    qs << "Internal Error - Could not create meshing operation";
    qWarning() << msg;
    QMessageBox::warning(pqCoreUtilities::mainWidget(), "Error", msg);
    return;
  }

  auto modelRes = std::dynamic_pointer_cast<smtk::model::Resource>(
    *(project->resources().findByRole(Metadata::roleToStr(Metadata::Role::MODEL)).begin()));
  smtk::model::Model model =
    modelRes->entitiesMatchingFlagsAs<smtk::model::Models>(smtk::model::MODEL_ENTITY)[0];
  if (!xmsGenMeshOp->parameters()->associateEntity(model))
  {
    QString msg;
    QTextStream qs(&msg);
    qs << "Internal Error - Could not generate mesh, see log view for details";
    qWarning() << msg;
    QMessageBox::warning(pqCoreUtilities::mainWidget(), "Error", msg);
    return;
  }
  // Configure Global Sizing constraint
  // Compute the in and max edge lengths
  double minEdgeLength = std::numeric_limits<double>::max();
  double maxEdgeLength = -std::numeric_limits<double>::max();
  // Compute smallest edge
  for (auto face : model.cellsAs<std::set<smtk::model::Face>>())
  {
    for (auto edge : face.edges())
    {
      // TODO: Make all edges hard edges
      auto vertices = edge.vertices();
      for (size_t i = 1; i < vertices.size(); i++)
      {
        double* vertA = vertices[i - 1].coordinates();
        double* vertB = vertices[i].coordinates();
        auto edgeLength = (vertA[0] - vertB[0]) * (vertA[0] - vertB[0]) +
          (vertA[1] - vertB[1]) * (vertA[1] - vertB[1]) +
          (vertA[2] - vertB[2]) * (vertA[2] - vertB[2]);
        minEdgeLength = std::min(minEdgeLength, edgeLength);
        maxEdgeLength = std::max(maxEdgeLength, edgeLength);
      }
    }
  }

  // Get sizing constraint from Mesher
  auto attRes = std::dynamic_pointer_cast<smtk::attribute::Resource>(
    *(project->resources().findByRole(Metadata::roleToStr(Metadata::Role::ANALYSIS)).begin()));
  auto att = attRes->findAttribute("MeshingConfiguration");
  auto sizingRatio = 0.1;
  if (att && att->isRelevant())
  {
    sizingRatio = att->findDouble("ratio")->value();
  }
  else
  {
    QString msg;
    QTextStream qs(&msg);
    qs << "Generating a mesh requires a Mesher to be selected and configured";
    qWarning() << msg;
    QMessageBox::warning(pqCoreUtilities::mainWidget(), "Error", msg);
    return;
  }

  // Clipping for the edge lengths to prevent near zero edges from overly refining the mesh edges
  double sizing = sizingRatio * std::sqrt(std::max(minEdgeLength, 0.01 * maxEdgeLength));

  auto gSizing = xmsGenMeshOp->parameters()->findDouble("Sizing");
  gSizing->setIsEnabled(true);
  gSizing->setValue(sizing);
  auto resultMesh = xmsGenMeshOp->operate();
  if (resultMesh->findInt("outcome")->value() != OP_SUCCESS)
  {
    QString msg;
    QTextStream qs(&msg);
    qs << "Internal Error - Could not generate mesh, see log view for details";
    qWarning() << msg;
    QMessageBox::warning(pqCoreUtilities::mainWidget(), "Error", msg);
    return;
  }
  auto meshRes = resultMesh->findResource("resource")->value();

  auto addOp = Add::create();
  addOp->parameters()->associate(project);
  addOp->parameters()->findResource("resource")->setValue(meshRes);
  addOp->parameters()->findString("role")->setValue(Metadata::roleToStr(Metadata::MESH));
  auto resultAdd = addOp->operate();
  if (resultAdd->findInt("outcome")->value() != OP_SUCCESS)
  {
    QString msg;
    QTextStream qs(&msg);
    qs << "Internal Error - Could add mesh to project, see log view for details";
    qWarning() << msg;
    QMessageBox::warning(pqCoreUtilities::mainWidget(), "Error", msg);
    return;
  }

  Q_EMIT this->meshGenerated();
}

} // namespace cmb2d
} // namespace simulation
} // namespace smtk
