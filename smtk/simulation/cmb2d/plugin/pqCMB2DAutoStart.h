#ifndef smtk_simulation_cmb2d_plugin_pqCMB2DAutoStart_h
#define smtk_simulation_cmb2d_plugin_pqCMB2DAutoStart_h

#include <QObject>

class pqServer;
class pqSMTKWrapper;

class pqCMB2DAutoStart : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  pqCMB2DAutoStart(QObject* parent = nullptr);
  ~pqCMB2DAutoStart() override;

  void startup();
  void shutdown();

protected Q_SLOTS:
  void resourceManagerAdded(pqSMTKWrapper* wrapper, pqServer* server);

private:
  Q_DISABLE_COPY(pqCMB2DAutoStart);
};

#endif
