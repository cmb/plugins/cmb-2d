//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#include "smtk/simulation/cmb2d/qt/qtProjectRuntime.h"

#include "smtk/io/Logger.h"
#include "smtk/project/Project.h"

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

qtProjectRuntime::qtProjectRuntime(QObject* parent)
  : Superclass(parent)
  , m_project(nullptr)
{
}

qtProjectRuntime::~qtProjectRuntime() {}

std::shared_ptr<smtk::project::Project> qtProjectRuntime::project() const
{
  return m_project;
}

void qtProjectRuntime::setProject(std::shared_ptr<smtk::project::Project> p)
{
  if (m_project)
  {
    smtkErrorMacro(
      smtk::io::Logger::instance(), "Internal Error: CMB-2D cannot manage multiple projects");
    return;
  }
  m_project = p;
}

void qtProjectRuntime::unsetProject(std::shared_ptr<smtk::project::Project>)
{
  m_project.reset();
}

} // namespace cmb2d
} // namespace simulation
} // namespace smtk
