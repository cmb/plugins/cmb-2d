//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_cmb2d_qt_qtProjectRuntime_h
#define smtk_simulation_cmb2d_qt_qtProjectRuntime_h

#include "smtk/simulation/cmb2d/qt/Exports.h"
#include "smtk/simulation/cmb2d/qt/qtInstanced.h"

#include "smtk/PublicPointerDefs.h"

#include <QObject>

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

/** \brief A singleton class for storing plugin runtime variables.
 *
 * The current plugin design only permits users to keep one smtk project
 * in memory at a time. This class stores a shared pointer to that
 * project instance. A QObject is used for possible inclusion as a
 * paraview "manager" for access by other plugins.
 *
  */
class SMTKCMB2DQTEXT_EXPORT qtProjectRuntime : public qtInstanced<qtProjectRuntime>
{
  Q_OBJECT
  using Superclass = qtInstanced<qtProjectRuntime>;
  friend Superclass;

public:
  smtk::project::ProjectPtr project() const;
  void setProject(smtk::project::ProjectPtr);
  void unsetProject(smtk::project::ProjectPtr);

protected:
  qtProjectRuntime(QObject* parent = nullptr);
  ~qtProjectRuntime() override;

private:
  smtk::project::ProjectPtr m_project;
};

} // namespace cmb2d
} // namespace simulation
} // namespace smtk

#endif
