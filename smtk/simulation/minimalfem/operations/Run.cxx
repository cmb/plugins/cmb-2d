#include "smtk/simulation/minimalfem/operations/Run.h"
#include "smtk/simulation/minimalfem/operations/Run_xml.h"

#include "smtk/simulation/minimalfem/operations/Export.h"

// SMTK
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/attribute/VoidItem.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/Operation.h"
#include "smtk/operation/XMLOperation.h"
#include "smtk/operation/operators/ImportPythonOperation.h"

// MinimalFEM
#include "MinimalFEM/MinimalFEM.h"

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

namespace
{
const int OP_SUCCEEDED = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);
}

#define LogInternalError(message)                                                                  \
  do                                                                                               \
  {                                                                                                \
    smtkErrorMacro(this->log(), "Internal Error: " << message);                                    \
    cleanup((int)smtk::operation::Operation::Outcome::FAILED);                                     \
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);                        \
  } while (0)

#define LogError(message)                                                                          \
  do                                                                                               \
  {                                                                                                \
    smtkErrorMacro(this->log(), "Error: " << message);                                             \
    cleanup((int)smtk::operation::Operation::Outcome::FAILED);                                     \
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);                        \
  } while (0)

namespace smtk
{
namespace simulation
{
namespace minimalfem
{

#define GetFileName(VAR, PARAM, EXT)                                                               \
  do                                                                                               \
  {                                                                                                \
    if (this->parameters()->findString(PARAM)->isSet())                                            \
    {                                                                                              \
      fs::path filename = this->parameters()->findString(PARAM)->value();                          \
      VAR = filename.replace_extension(EXT);                                                       \
    }                                                                                              \
    else                                                                                           \
    {                                                                                              \
      VAR = defaultFileName + EXT;                                                                 \
    }                                                                                              \
  } while (0)

enum OutputFormat
{
  MFEM_OUT = 0,
  VTP = 1
};

Run::Result Run::operateInternal()
{
  std::function<void(int)> cleanup = [](int) {};

  static const std::string defaultFileName = "minimalFEM";

  // Get the input/output file names
  fs::path runDir = this->parameters()->findDirectory("run-directory")->value();

  int formatInt = this->parameters()->findInt("out-format")->value();
  std::string format = "vtp";
  switch (formatInt)
  {
    case MFEM_OUT:
      format = "out";
      break;
    case VTP:
      format = "vtp";
      break;
  }

  fs::path inpFileName;
  fs::path outFileName;
  GetFileName(inpFileName, "inp-filename", ".inp");
  GetFileName(outFileName, "out-filename", "." + format);

  fs::path inpFile = runDir / inpFileName;
  fs::path outFile = runDir / outFileName;

  // Check if the input file exists
  if (!fs::exists(inpFile))
  {
    LogError("No input located in run directory: " << inpFile.string());
  }

  // Check for overwrite
  bool allowOverwrite = this->parameters()->findVoid("overwrite")->isEnabled();
  if (!allowOverwrite)
  {
    if (fs::exists(outFile))
    {
      LogError("Output file exists, cannot run");
    }
  }

  ::minimalfem::Solver(inpFile.string(), outFile.string(), "vtp");

  return this->createResult(smtk::operation::Operation::Outcome::SUCCEEDED);
}

const char* Run::xmlDescription() const
{
  return Run_xml;
}

} // namespace minimalfem
} // namespace simulation
} // namespace smtk
