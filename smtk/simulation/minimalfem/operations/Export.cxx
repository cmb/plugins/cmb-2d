#include "smtk/simulation/minimalfem/operations/Export.h"
#include "smtk/simulation/minimalfem/operations/Export_xml.h"

// SMTK
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/Operation.h"
#include "smtk/operation/XMLOperation.h"
#include "smtk/operation/operators/ImportPythonOperation.h"

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

namespace
{
const int OP_SUCCEEDED = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);
}

#define LogInternalError(message)                                                                  \
  do                                                                                               \
  {                                                                                                \
    smtkErrorMacro(this->log(), "Internal Error: " << message);                                    \
    cleanup((int)smtk::operation::Operation::Outcome::FAILED);                                     \
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);                        \
  } while (0)

#define LogError(message)                                                                          \
  do                                                                                               \
  {                                                                                                \
    smtkErrorMacro(this->log(), "Error: " << message);                                             \
    cleanup((int)smtk::operation::Operation::Outcome::FAILED);                                     \
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);                        \
  } while (0)

namespace smtk
{
namespace simulation
{
namespace minimalfem
{

Export::Result Export::operateInternal()
{
  std::function<void(int)> cleanup = [](int) {};

  std::string wfd = this->parameters()->findDirectory("workflow-directory")->value();
  fs::path workflowDir(wfd);
  fs::path pyPath = workflowDir / "MinimalFEM.py";
  if (!fs::exists(pyPath))
  {
    LogError("Expecting python file at " << pyPath.string());
  }

  auto importPythonOp = this->manager()->create<smtk::operation::ImportPythonOperation>();
  if (!importPythonOp)
  {
    LogInternalError("Unable to create ImportPythonOperation");
  }
  importPythonOp->parameters()->findFile("filename")->setValue(pyPath.string());
  auto pyResult = importPythonOp->operate();
  if (pyResult->findInt("outcome")->value() != OP_SUCCEEDED)
  {
    LogError("Failed to load python export script: " << pyPath.string());
    int outcome = pyResult->findInt("outcome")->value();
    return this->createResult(static_cast<Export::Outcome>(outcome));
  }
  printf("Here\n");
  auto operationName = pyResult->findString("unique_name")->value();
  printf("Here now\n");

  auto opManager = this->manager();
  cleanup = [opManager, operationName](int) { opManager->unregisterOperation(operationName); };

  auto exportOp = this->manager()->create(operationName);
  if (!exportOp)
  {
    LogError("Failed to instantate operation from export script");
  }

  auto analysisResource = this->parameters()->findResource("analysis")->value();
  auto meshResource = this->parameters()->findResource("mesh")->value();
  auto filename = this->parameters()->findFile("filename")->value();

  exportOp->parameters()->findResource("attributes")->setValue(analysisResource);
  exportOp->parameters()->findResource("mesh")->setValue(meshResource);
  exportOp->parameters()->findFile("filename")->setValue(filename);
  auto exportResult = exportOp->operate();
  int thisOutcome = exportResult->findInt("outcome")->value();

  cleanup(thisOutcome);

  return this->createResult(static_cast<smtk::operation::Operation::Outcome>(thisOutcome));
}

const char* Export::xmlDescription() const
{
  return Export_xml;
}

} // namespace minimalfem
} // namespace simulation
} // namespace smtk
