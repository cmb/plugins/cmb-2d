add_subdirectory(cmb2d)

option(ENABLE_MinimalFEM "Enable MinimalFEM operations" ON)
if (ENABLE_MinimalFEM)
  add_subdirectory(minimalfem)
endif ()
