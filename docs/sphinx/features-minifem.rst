Running the MinimalFEM Solver
=============================

In most applications, the finite element code is run separately, outside of modelbuilder, possibly on a different machine (desktop, network cluster, HPC system, or cloud system). For convenience, we have embedded the MinimalFEM solver into ModelBuilder-2D so that you can run it directly. (Our problem set is sufficiently small that MinimalFEM can run on a standard desktop machine.)

.. _run-minifem:

.. rst-class:: step-number

17\. Run MinimalFEM

Clear the :guilabel:`Output Messages` view again by clicking the :guilabel:`Clear` button. Then go to the :guilabel:`CMB2D` menu and select the :guilabel:`Run Solver` option. In response, ModelBuilder-2D will load the ``minfem.inp`` file generated in the previous step, execute the MinimalFEM solver, read the native MinimalFEM output, and translate the data into a VTK *polydata* file that is named ``minfem.vtp``. The code to convert the result to .vtp file format was written by Kitware to make it easy to ingest for visualization. This code was added to a fork of the origin MinimalFEM repository; you can access the MinimalFEM source that includes our additions at `Our GitLab repository <https://gitlab.kitware.com/third-party/minimalfem>`_.

As with meshing, the solver should run in 1-2 seconds, and the only user feedback are info messages that the operation succeeded.
