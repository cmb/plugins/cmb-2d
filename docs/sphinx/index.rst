######################
ModelBuilder-2D
######################

:program:`ModelBuilder-2D` is a customized version of the open source `CMB <https://computationalmodelbuilder.org/>`_ ModelBuilder simulation preprocessing application for numerical simulation. To provide general familiarization with CMB, this version of modelbuilder is designed to construct and carry out basic numerical simulations with two-dimensional geometry. In paricular, this version of modelbuilder includes a two-dimensional finite-element solver for flat stress problems. Users can input model geometry by creating and importing text files that describe planar polygon shapes. Model edge and vertex entities can be assigned simulation *attributes* to specify loads and displacement constraints. Users can generate unstructured triangle meshes from geometric models, generate solver input files, run the solver, and visualize the computed results (stress and displacement fields) from the modelbuilder user interface.



Contents:

.. toctree::
   :maxdepth: 2

   install.rst
   features.rst

.. image:: ./images/cmb2d-splash.png


Note that the ModelBuilder/CMB platform is not tied to any specific modeling, meshing, or simulation software, but instead is designed so that it can be customized for nearly any simulation toolset and workflow. In this particular implementation, we have integrated three open-source software libraries for demonstration:

* The `Boost Polygon library <https://www.boost.org/doc/libs/1_73_0/libs/polygon/doc/index.htm>`_ provides the geometry manipulations for the CMB modeling subsystem.
* The `Aquaveo XMS Mesher <https://github.com/Aquaveo/xmsmesher>`_ is used to generate two-dimensional unstructured meshes from polygons.
* And the `MinimalFEM <https://github.com/podgorskiy/MinimalFem>`_ solver is used for the  finite-element calculations.


..
    ##################
    Indices and tables
    ##################

    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`
