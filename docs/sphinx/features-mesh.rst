Generating the Mesh
===================

To provide two-dimensional meshing, we embedded the open source `Aquaveo XMS Mesher <https://github.com/Aquaveo/xmsmesher>`_ into ModelBuilder-2D. XMS Mesher is a full-feature tool for geometry discretization with features such as edge and line refinement, vertex and point refinement, and linear sizing functions. For this walkthrough, however, we only expose a global mesh size (constant). (For those interested, the full XMS feature set can be accessed through the modelbuilder operation view; the operation name is ``smtk::mesh::xms::GenerateMesh``).

.. _add-renderview:

.. rst-class:: step-number

11\. Add a RenderView

To start, we are going to add a second view to display the results of the meshing operation. There are several ways to do this; for this walkthrough we are going to create a separate layout. In the tab bar above the current RenderView, find and click the :guilabel:`+` button to create a second tab. A new tab labeled :guilabel:`Layout #2` will be displayed, showing a :guilabel:`Create View` label and a vertical list of buttons. Click the :guilabel:`Render View` button, which is the first one in the list, which will display the geometry model.

.. image:: ./images/toolbutton-new-layout.png

In this new layout we are going to do two things: (i) reset the camera to be centered and scaled to our geometry, then (ii) hide the model. First, find the camera :guilabel:`Reset` button in the main toolbars and click on it, which will update the display so that you can see the geometric model centered in the view.

.. image:: ./images/toolbutton-camera-reset.png

Then right-click over the model and select the ``Hide``` item in the context menu, leaving an empty view. You can also rename each tab by right-clicking over the :guilabel:`Layout #` label and selecting the ``Rename`` option. Use that to rename :guilabel:`Layout #1` to ``Model`` and :guilabel:`Layout #2` to ``Mesh``.

.. image:: ./images/second-layout.png


.. rst-class:: step-number

12\. Select the Mesher

Although XMS is the only mesher currently available in ModelBuilder-2D, you must still select it in the :guilabel:`Analysis` tab. In the :guilabel:`Attribute Editor`, click on the :guilabel:`Analysis` tab and find the selector next to the :guilabel:`Mesher` label, and set it to ``XMSMesher``. (We could have done this any time after creating the project, of course.)

.. image:: ./images/mesher-selected.png


.. rst-class:: step-number

13\. Set the Mesh Size

When you set the mesher, a new tab "Meshing Configuration" appears in the :guilabel:`Attribute editor`. Click on the :guilabel:`Meshing Configuration`` tab to see it has one field labeled :guilabel:`Global Sizing Constraint (Relative)`. You can enter a number in the range ``(0.0, 1.0)`` to control the number of mesh elements that get created. Enter ``0.25`` and hit the :kbd:`<Enter>` key. You should see the field's background change from yellow to white, indicating that the value you entered is different than the default.

.. image:: ./images/meshsize-set.png


.. rst-class:: step-number

14\. Generate the Mesh

With the global mesh size set to ``0.25``, go to the :guilabel:`CMB2D` menu and select the :guilabel:`Generate Mesh` item. In response, modelbuilder will input the project's model resource to the XMS mesher, run the meshing operation, translate the meshing results into a new *mesh resource* and add that mesh resource to the project. When this is complete (should only take a few seconds), ModelBuilder-2D will display the mesh resource. Click the :guilabel:`Camera Reset` button to again center the view. To see the mesh elements, right-click over the object and in the context menu change the :guilabel:`Representation` from ``Surface`` to ``Surface With Edges``.

.. image:: ./images/mesh-generated.png


.. rst-class:: step-number

15\. Save the Project

With the mesh now generated and added to the project, let's again write the project changes to your file system. Go to the :guilabel:`CMB2D` menu and select the :guilabel:`Save Project` item.
