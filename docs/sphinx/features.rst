*******************
Feature Walkthrough
*******************

In this section, we walk though a basic numerical stress/strain problem to demonstrate the functional capabilities of ModelBuilder: importing model geometry, assigning boundary conditions and material properties, generating meshes, generating solver input files, running a solver, and visualizing the results. The geometry we'll use for this problem is a convex polygon with one inner loop (hole). We apply a two loads, one distributed land one point load, and calculate the resulting stress and strain fields.

.. image:: ./images/example-sketch.svg


.. toctree::

    features-project.rst
    features-model.rst
    features-analysis.rst
    features-bcs.rst
    features-mesh.rst
    features-export.rst
    features-minifem.rst
    features-view.rst
    features-close.rst
