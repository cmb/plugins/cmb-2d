Closing the Project
===================

.. rst-class:: step-number

24\. Close the Project

When you are done exploring the results, go to the "CMB2D" menu and select the "Close Project" item to remove it from memory and clear the corresonding view panels. You will need to manually delete the visualization items in the Pipeline Browser, because that data is not considered part of the project. You can do this by first selecting each of the items under ``minfem.vtp`` in the Pipeline Browser and hitting the ``<Delete>`` key (or you can right-click and select the "Delete" item in the context menu). Then select the ``minfem.vtp`` item itself and hit the ``<Delete>`` key.
