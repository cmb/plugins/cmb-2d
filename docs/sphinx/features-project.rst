Creating a Project
==================

.. figure:: images/cmb2d-menu.png
    :figwidth: 40%
    :align: right

Most menu interactions will be with the :guilabel:`CMB2D` project menu, which has standard features to open ModelBuilder projects, create projects, save projects, and close projects. It also includes features specific to this tutorial for creating and editing project resources and using them to run the analysis code. For persistent storage,  project resource files are stored in a user-specified directory on the available filesystem.


.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

1\. Create a Project

Although each of the features described in the following sections can be carried out in a standalone fashion, :program:`ModelBuilder-2D` includes a *project* feature to simplify the data management required by end users. To begin, go to the :guilabel:`CMB2D` menu and select the :guilabel:`New Project...` item. In response, modelbuilder displays a file system dialog for selecting a directory to use for storing project data. Use the "New Folder" icon to create a project directory in a convenient location, for example, ``<home-directory>/modelbuilder/projects/cmb2d-example``. In practice, you also use an existing directory --- if it contains any files or subdirectories, modelbuilder will prompt you to confirm that it is OK to delete the contents of that directory before proceeding.

Once you click the :guilabel:`OK` button, a second dialog will appear with a few options. For now, just click the :guilabel:`Apply` button. When that dialog closes, modelbuilder configures the project with an *attribute resource* for specifying the analysis problem (material properties, displacement constraints, and loads). The user interface for editing these properties is displayed in the :guilabel:`Attributes Editor` tab. The contents of the :guilabel:`Attribute Editor` -- tabs and editing fields -- are specified by XML (text) file read in my by modelbuilder when the project is created. The XML *template file* used in this tutorial can be found under the installed ModelBuilder-2D directory. Look for the filename ``MinimalFEM.sbt``. More details on the file format can be found at this `Template File Syntax <https://smtk.readthedocs.io/en/latest/userguide/attribute/file-syntax.html/>`_ link.

.. image:: ./images/new-project.png


.. rst-class:: step-number

2\. Set RenderView to 2D mode

Be default, :program:`ModelBuilder` applications display geometric models in three dimensions. Because we will be using  two-dimensional geometry throughout this tutorial, make sure that the render view is set to its 2D interaction mode. To do this, look in the RenderView toolbar for a button labeled either ``3D`` or ``2D``. If the label is ``3D`` click it once to toggle it to ``2D``.

.. image:: images/interaction-mode.png

|
