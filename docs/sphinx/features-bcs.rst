Assigning Boundary and Load Conditions
========================================

We next add boundary conditions and loading specifications to selected edges and vertices in the geometric model.


.. rst-class:: step-number

8\. Add Displacement Constraints

The only boundary conditions used in our :program:`MinimalFEM` analysis are displacement constraints. For our problem, we will constrain two edges: the bottom edge and the adjacent one to its left. To do this, click on the :guilabel:`Constraints` tab. The user interface in this tab is an *attribute view*, in which you create attributes as needed for your specfication. For this problem, only one attribute is needed; to add it, click the :guilabel:`New` button to the right of the :guilabel:`Constraint` label, and note that an item labeled ``constraint-0`` appears in a list below the button. You can double click the name to edit it, but the default name is fine.

Below the list of attributes is a :guilabel:`Displacement Constraint` field with a selection box to choose the direction, with the choices ``X Direction (default)``, ``Y Direction``, or ``X and Y Directions``. Choose ``X and Y Directions`` from the list.

Below that is a :guilabel:`Model Boundary and Attribute Associations` widget -- this is where you assign the displacement constraint attribute to model entities. For this problem, select ``edge 01`` and ``edge 02`` in the :guilabel:`Available` list, then click the arrow to move those entities to the left-hand list (:guilabel:`Currrent`).

.. image:: ./images/constraints-set.png


.. rst-class:: step-number

9\. Set Loads

For our problem, we will apply two load conditions, one to the top-most edge and the other to a vertex on the right-hand size. To do this, click on the :guilabel:`Loads` tab to display its attribute view. In this view, we will create two attributes, one for each load.

* Click the :guilabel:`New` button to create the first load attribute. Below that, find the :guilabel:`Force (N)` fields and set the :guilabel:`Y-Force` to ``-3.5``. In the :guilabel:`Available` list, select ``edge 06`` and move it to the :guilabel:`Current` list.
* Click the :guilabel:`New` button to create the second load attribute. For the :guilabel:`Force (N)` fields set :guilabel:`X-Force` to ``-2.0`` and :guilabel:`Y-Force` to ``-2.0``. In the :guilabel:`Available` list, select ``vertex 04`` and move it to the :guilabel:`Current` list.

.. image:: ./images/loads-set.png


.. rst-class:: step-number

10\. Save the Project

We have now specified all of the information needed for our problem. Save the project once again by going to the :guilabel:`CMB2D` menu and selecting the :guilabel:`Save Project` item. The next step is meshing.
