Installing ModelBuilder-2D
-------------------------------

.. note:: At present, the ModelBuilder-2D install package is only available for linux systems.

.. rst-class:: step-number

1\. Download the Package File

The latest release packages for :program:`ModelBuilder-2D` are stored online in the `CMB-2D/Downloads Folder <https://data.kitware.com/#collection/58fa68228d777f16d01e03e5/folder/6255da864acac99f420a8c57>`_. Navigate a web browser to that link, locate the package file for your platform, and click the download button to the right of it.

.. rst-class:: with-border
.. image:: ./images/cmb2d-download-page.png
    :align: center

|

.. rst-class:: step-number

2\. Extract the Package File

After the download is complete:

* For linux, extract the files from the :file:`.tar.gz` package to a convenient folder on your system. The modelbuilder executable is in the ``bin`` folder.

.. * For macOS, open the ``.dmg`` package and drag the :program:`modelbuilder.app` icon to a folder on your system. Because we don't distribute :program:`ModelBuilder for ACE3P` through Apple's App Store, macOS will display warning dialogs when you unpack the ``.dmg`` file and start the application for the first time.
.. * For Windows, unzip the ``.zip`` package to a convenient folder. The modelbuilder executable is in the ``bin`` folder.


.. rst-class:: step-number

3\. Run the Executable

When you first start :program:`ModelBuilder-2D`, it looks similar to the :program:`ParaView` desktop application, with the main differences being that (i) some of the :program:`ParaView` toolbars have been hidden, and (ii) the left sidebar has two new dock widgets labeled :guilabel:`Resources` and :guilabel:`Attribute Editor`, and (iii) there is a new :guilabel:`CMB2D` project menu.

.. image:: ./images/modelbuilder-2d-installed.png
    :align: center

|

.. rst-class:: step-number

4\. Initialize Settings

Before going further, there are a couple application settings that are particularly useful for modelbuilder workflows. To set them, open the settings panel

* On Linux and Windows systems, go to the :guilabel:`Edit` menu and select the :guilabel:`Settings...` item.
* On macOS systems, go to the :guilabel:`modelbuilder` menu and select the :guilabel:`Preferences...` item.

Then in the :guilabel:`Settings` dialog, select the :guilabel:`Sim Pre-processing` tab.

1. Find the :guilabel:`Hightlight On Hover` item and check the box.
2. Find the :guilabel:`Resource Tree Style` item and change the selector to ``two-level``.

These settings do not take effect until the next time you start modelbuilder, so go ahead an exit by going to the :guilabel:`File` menu and selecting the :guilabel:`Exit` item (on macOS, go to the :guilabel:`modelbuilder` menu and select the :guilabel:`Quit modelbuiler` item.

.. figure:: images/annotated-smtk-settings.png
    :figwidth: 60%
    :align: center

|
