# A contract file for testing ACE3P extensions within SMTK.

cmake_minimum_required(VERSION 3.20)
project(cmb2d)

include(ExternalProject)

if (NOT DEFINED SMTK_CONTRACT_TAG)
  set(SMTK_CONTRACT_TAG "origin/master")
endif()

ExternalProject_Add(cmb2d
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/plugins/cmb-2d.git"
  GIT_TAG "${SMTK_CONTRACT_TAG}"
  PREFIX plugin
  STAMP_DIR plugin/stamp
  SOURCE_DIR plugin/src
  BINARY_DIR plugin/build
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -DENABLE_TESTING=ON
    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
    -Dsmtk_DIR=${smtk_DIR}
    ${response_file}
  INSTALL_COMMAND ""
  TEST_BEFORE_INSTALL True
)
